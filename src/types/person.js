// @flow
export type Person = {
  name?: string,
  uuid: string,
  role?: string | number,
  date?: string
};
